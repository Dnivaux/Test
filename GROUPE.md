# Groupes participant au projet

Chaque groupe possède:

 * un nom
 * 3 à 4 membres, identifiés par leurs noms, prénoms, et logins sur [Codeberg](https://codeberg.org)
 * un sous-répertoire du projet, avec les fichiers du groupe, et dont le nom est directement dérivé de celui du groupe 

Ces éléments pour chaque groupe sont décrits dans une sous-section de ce fichier en suivant le modèle du groupe fictif _Exemple_ ci-dessous.

## Exemple

Le groupe _Exemple_ (sous-répertoire `exemple`) est composé de:

 * Franck Pommereau [fpom](https://codeberg.org/fpom)
 * et c'est tout car c'est juste un exemple
 * mais en vrai il faudrait au moins trois membres
 * (et au plus quatre)
 *Daë Nivaux

Nous avons traité _(décrire ici le sujet choisi pour le rapport et l'exposé)_.